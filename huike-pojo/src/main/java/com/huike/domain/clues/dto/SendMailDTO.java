package com.huike.domain.clues.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author：lizhijie
 * @Date：2023/7/5 20:34
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SendMailDTO {
    private String date;
    private Long newClueCount;
    private Long newBusinessCount;
    private Long newContractCount;
    private Long saleMoney;
}
