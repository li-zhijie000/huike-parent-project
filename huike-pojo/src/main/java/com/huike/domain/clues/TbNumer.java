package com.huike.domain.clues;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author：lizhijie
 * @Date：2023/7/5 10:28
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TbNumer {
    private String date;
    private Long count;
}
