package com.huike.utils.mail;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import javax.mail.internet.MimeMessage;
import java.io.File;

public class SendMailUtils {

    private JavaMailSender javaMailSender;

    public SendMailUtils(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }






    public void testSendMail() throws Exception {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom("2798696233@qq.com");
        mimeMessageHelper.setTo("zhangruizhe0912@foxmail.com");
        mimeMessageHelper.setCc("232322323@163.com"); //抄送
        mimeMessageHelper.setBcc("1500317030@qq.com"); //密送
        mimeMessageHelper.setSubject("Mail测试邮箱");

        mimeMessageHelper.setText("<h1>这是一封测试邮件 .....<h1>" , true);
        mimeMessageHelper.addAttachment("1.jpg", new File("D:\\picture\\picture\\fda4c422783ee9f6dd5603b17ec2d188.png"));
        mimeMessageHelper.addAttachment("2.pdf", new File("D:\\java\\项目1\\客达天下-课程资料\\客达天下-课程资料\\08. 其他资料\\合同.pdf"));
        javaMailSender.send(mimeMessage);
    }
	
}