package com.huike.utils.mail;

import com.huike.common.constant.MessageConstants;
import com.huike.common.exception.CustomException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

public class EmailUtils {

    private JavaMailSender javaMailSender;

    public EmailUtils(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMailWithoutAttachment(String from, String to,  String subject, String text, String ... cc) throws Exception {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
        messageHelper.setFrom(from);//发件人
        messageHelper.setTo(to);//收件人
        if(cc != null && cc.length>0){
            messageHelper.setCc(cc);//抄送人
        }
        messageHelper.setSubject(subject);//主题
        messageHelper.setText(text , true);//内容
        javaMailSender.send(mimeMessage);
    }
    //发送带有附件的邮件
    public void sendMailWithAttachment(String from, String to,  String subject, String attachName,String filePath,String text, String ... cc) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom(from);//发件人
        mimeMessageHelper.setTo(to);//收件人
        if(cc != null && cc.length>0){
            mimeMessageHelper.setCc(cc);//抄送人
        }
        mimeMessageHelper.setSubject(subject);//主题
        mimeMessageHelper.addAttachment(attachName, new File(filePath));
        mimeMessageHelper.setText(text , true);//内容
        javaMailSender.send(mimeMessage);

        return;



    }

}