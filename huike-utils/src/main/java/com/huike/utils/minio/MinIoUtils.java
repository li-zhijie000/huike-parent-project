package com.huike.utils.minio;

import com.huike.common.config.MinioConfig;
import com.huike.domain.common.AjaxResult;
import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @Author：lizhijie
 * @Date：2023/6/23 22:48
 * @Description:
 */
@Slf4j
@AllArgsConstructor
public class MinIoUtils {
    /**
     * 文件上传至Minio
     * 使用try catch finally进行上传
     * finally里进行资源的回收
     */
    private String endpoint;
    private int port;
    private String accessKey;
    private String secretKey;
    private Boolean secure;
    private String bucketName;
    public AjaxResult upload(MultipartFile file) {

        InputStream inputStream = null;
        //创建Minio的连接对象
        MinioClient minioClient = getClient();
        //桶对象
        try {
            inputStream = file.getInputStream();
            //基于官网的内容，判断文件存储的桶是否存在 如果桶不存在就创建桶
            boolean exists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
            if (exists) {
                System.out.println("该桶已经存在");
            } else {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            }
            /**
             * ================================操作文件================================
             * 思路：我们上传的文件是:文件.pdf
             * 那么我们应该上传到配置的bucket内 我们配置的bucketName是name
             * 那么我们存在桶里的文件应该是什么样的 也叫“文件.pdf”吗?
             * 应该按照上传的年月日进行区分
             * 举例：2021-05-05日进行上传的
             * 那么存在桶里的路径应该是
             * name/2021/05/05/这个目录下
             * 而对于同一个文件，存在重名问题，所以我们应该利用UUID生成一个新的文件名，并拼接上 .pdf 作为文件后缀
             * 那么完整的路径就是 name/2021/05/05/uuid.pdf
             *
             * 如果上述思路你无法理解，那么就直接存放在桶内生成uuid+.pdf即可
             * 即：name/uuid.pdf
             */
            //操作文件
            String fileName = file.getOriginalFilename();
            String objectName = new SimpleDateFormat("yyyy/MM/dd/").format(new Date()) + UUID.randomUUID().toString().replaceAll("-", "")
                    + fileName.substring(fileName.lastIndexOf("."));
            PutObjectArgs objectArgs = PutObjectArgs.builder().object(objectName)
                    .bucket(bucketName)
                    .contentType(file.getContentType())
                    .stream(file.getInputStream(), file.getSize(), -1).build();
            minioClient.putObject(objectArgs);
            //封装访问的url给前端
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", "/" + bucketName + "/" + objectName);
            //url需要进行截取
            ajax.put("url", endpoint + ":" +port + "/" + bucketName + "/" + objectName);
            /**
             * 构建返回结果集
             */

            /**
             * 封装需要的数据进行返回
             */
            return ajax;
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("上传失败");
        } finally {
            //防止内存泄漏
            if (inputStream != null) {
                try {
                    inputStream.close(); // 关闭流
                } catch (IOException e) {
                    log.debug("inputStream close IOException:" + e.getMessage());
                }
            }
        }
    }


//这个方法里面的属性用了一个配置文件实体类（minioConfig）（也就是一些minio的连接属性）
//属性自己看文档定义
    /**
     * 免费提供一个获取Minio连接的方法
     * 获取Minio连接
     * @return
     */
    private MinioClient getClient() {
        MinioClient minioClient =
                MinioClient.builder()
                        .endpoint("http://" + endpoint + ":" + port)
                        .credentials(accessKey, secretKey)
                        .build();
        return minioClient;
    }

}
