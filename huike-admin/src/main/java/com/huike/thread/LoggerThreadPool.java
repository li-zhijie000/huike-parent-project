package com.huike.thread;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PreDestroy;
import java.util.concurrent.*;

@Configuration
@Slf4j
@EnableAsync//开启异步任务
public class LoggerThreadPool {
    private ThreadPoolTaskExecutor threadPoolTaskExecutor = null;
    private ThreadPoolExecutor threadPoolExecutor = null;
//    private final static int POOL_SIZE = 5;  // 线程池大小
//    private static ExecutorService executorService;
//
//    private LoggerThreadPool() {} // 禁止实例化
//
//    // 获取线程池单例
//    public static synchronized ExecutorService getExecutor() {
//        if (executorService == null) {
//            executorService = new ThreadPoolExecutor(
//                    POOL_SIZE,
//                    POOL_SIZE,
//                    0L,
//                    TimeUnit.MILLISECONDS,
//                    new LinkedBlockingQueue<>(),
//                    new ThreadPoolExecutor.CallerRunsPolicy()
//            );
//        }
//        return executorService;

    @Bean("springThread")
    public TaskExecutor taskExecutor(){
        log.info("spring的线程池");
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(10);
        taskExecutor.setMaxPoolSize(20);
        taskExecutor.setKeepAliveSeconds(300);
        taskExecutor.setQueueCapacity(10);
        taskExecutor.setThreadNamePrefix("Huike-Thread-pool-");
        return taskExecutor;
    }
    @PreDestroy
    public void shutdown(){
        log.info("服务器关闭, 释放资源,  关闭线程池");
        if(threadPoolExecutor != null){
            log.info("服务器关闭, 释放资源,  关闭线程池threadPoolExecutor");
            threadPoolExecutor.shutdown();
        }
        if(threadPoolTaskExecutor != null){
            log.info("服务器关闭, 释放资源,  关闭线程池threadPoolTaskExecutor");
            threadPoolTaskExecutor.shutdown();
        }
    }
}