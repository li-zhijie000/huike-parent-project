package com.huike.aspectj;

import com.huike.common.annotation.PreAuthorize;
import com.huike.common.constant.HttpStatus;
import com.huike.common.constant.MessageConstants;
import com.huike.common.exception.CustomException;
import com.huike.web.service.PermissionService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.net.http.HttpResponse;

/**
 * @Author：lizhijie
 * @Date：2023/6/24 22:16
 * @Description:
 */
@Component
@Aspect
public class RoleAspect {
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private HttpServletResponse httpResponse;
    @Around("@annotation(preAuthorize)&& execution(* com.huike.controller.*.*.*(..))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint,PreAuthorize preAuthorize) throws Throwable {
        if (permissionService.hasPermission(preAuthorize.value())){
            return proceedingJoinPoint.proceed();
        }else {
            httpResponse.setStatus(HttpStatus.UNAUTHORIZED);
            return null;
        }
    }
}
