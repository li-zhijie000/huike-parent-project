package com.huike.aspectj;

import com.alibaba.fastjson.JSONObject;
import com.huike.common.annotation.Log;
import com.huike.common.exception.CustomException;
import com.huike.domain.system.SysOperLog;
import com.huike.domain.system.dto.LoginUser;
import com.huike.service.ISysOperLogService;
import com.huike.service.impl.SysOperLogServiceImpl;
import com.huike.utils.ip.AddressUtils;
import com.huike.utils.ip.IpUtils;
import com.huike.web.service.SysLoginService;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;

/**
 * @Author：lizhijie
 * @Date：2023/7/1 10:14
 * @Description:
 */
@Component
@Aspect
@Slf4j
public class LogAspect2 {
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ISysOperLogService sysOperLogService;

    @Around("@annotation(myLog) && execution(* com.huike.controller.*.*.*(..))")
    public Object log(ProceedingJoinPoint proceedingJoinPoint ,Log myLog) throws Throwable {
        log.info("记录日志");
        try {
            Object proceed = proceedingJoinPoint.proceed();
            handleLog(proceedingJoinPoint,myLog,proceed,null);
            return proceed;
        } catch (Throwable throwable) {
            handleLog(proceedingJoinPoint,myLog,null,throwable);
            throwable.printStackTrace();
            throw new CustomException(throwable.getMessage()); //继续往上抛
        }
    }


    //记录日志方法
    private void handleLog(JoinPoint joinPoint, Log myLog, Object result, Throwable throwable) {
        SysOperLog sysOperLog = new SysOperLog();
        sysOperLog.setTitle(myLog.title());
        sysOperLog.setBusinessType(myLog.businessType().ordinal());
        sysOperLog.setMethod(joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName());
        sysOperLog.setRequestMethod(request.getMethod());
        LoginUser loginUser = tokenService.getLoginUser(request);
        if(loginUser != null){
            sysOperLog.setOperName(loginUser.getUser().getUserName());
            sysOperLog.setDeptName(loginUser.getUser().getDept() != null? loginUser.getUser().getDept().getDeptName():null);
        }
        sysOperLog.setOperUrl(request.getRequestURI());
        String ipAddr = IpUtils.getIpAddr(request);
        sysOperLog.setOperIp(ipAddr);
        String location = AddressUtils.getRealAddressByIP(ipAddr);
        sysOperLog.setOperLocation(location);
        sysOperLog.setOperParam(Arrays.toString(joinPoint.getArgs()));
        sysOperLog.setJsonResult(JSONObject.toJSONString(result));
        sysOperLog.setOperTime(new Date());
        sysOperLog.setStatus(0);
        if(throwable != null){
            sysOperLog.setStatus(1);
            sysOperLog.setErrorMsg(throwable.getMessage());
        }
        sysOperLogService.insertOperlog(sysOperLog);
    }

}
