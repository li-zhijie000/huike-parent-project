package com.huike.config;

import com.huike.utils.mail.EmailUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.internet.MimeMessage;

/**
 * @Author：lizhijie
 * @Date：2023/7/1 15:25
 * @Description:
 */
@Configuration
public class MailConfig {
    //进行自动配置
    @Bean
    public EmailUtils emailUtils(JavaMailSender javaMailSender){
        return new EmailUtils(javaMailSender);
    }

}
