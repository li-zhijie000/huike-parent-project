package com.huike.config;

import com.huike.common.config.MinioConfig;
import com.huike.utils.minio.MinIoUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：lizhijie
 * @Date：2023/6/24 16:08
 * @Description:
 */
@Configuration
public class CommonConfig {
    @Bean
    public MinIoUtils create(MinioConfig minioConfig){
        return new MinIoUtils(minioConfig.getEndpoint(),minioConfig.getPort(),minioConfig.getAccessKey(),
                minioConfig.getSecretKey(),minioConfig.getSecure(),minioConfig.getBucketName());
    }
}
