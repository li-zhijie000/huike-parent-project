package com.huike.interceptor;

import com.huike.common.constant.HttpStatus;
import com.huike.domain.system.dto.LoginUser;
import com.huike.utils.StringUtils;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @Author：lizhijie
 * @Date：2023/6/22 2:03
 * @Description:
 */
@Component
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {
    @Autowired
    private TokenService tokenService;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
     //首先拿到令牌
        String token = tokenService.getToken(request);
        //判断令牌是否为空
        if (StringUtils.isEmpty(token)){
            log.info("令牌为空");
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }
        //判断令牌是否合法
        try {
            tokenService.parseToken(token);
        } catch (Exception e) {
           log.info("令牌非法");
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }
        //判断令牌有效期
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (Objects.isNull(loginUser)){
            log.info("令牌过期");
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }
        //令牌续期
        tokenService.refreshAndCacheToken(loginUser);
        return true;
    }
}
