package com.huike.task;

import com.huike.domain.business.TbBusiness;
import com.huike.domain.clues.ContarctMoney;
import com.huike.domain.clues.TbClue;
import com.huike.domain.clues.TbNumer;
import com.huike.domain.clues.dto.SendMailDTO;
import com.huike.domain.system.SysUser;
import com.huike.mapper.*;
import com.huike.utils.file.FileUtils;
import com.huike.utils.mail.EmailUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.management.relation.Role;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @Author：lizhijie
 * @Date：2023/7/1 10:45
 * @Description:
 */
@Component
@Slf4j
public class MailTask {
    @Autowired
    private TbClueMapper tbClueMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private TbBusinessMapper tbBusinessMapper;

    private static final String CLUE_NOTIFY_PREFIX = "clue:notify:";
    private static final String BUSINESS_NOTIFY_PREFIX = "business:notify:";
    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;
    @Autowired
    private EmailUtils emailUtils;

    @Value("${spring.mail.username}")
    private String mailFrom;
    //发送线索邮件
    //@Scheduled(cron =" 0 0/2 * * * ?")
    public void sendMail() throws Exception {
        log.info("开始发送邮件");
        //对于是否有满足要求的sql进行查询
     List<TbClue> tbClues= tbClueMapper.selectMailCondition(LocalDateTime.now(),LocalDateTime.now().plusHours(2));
     //对于返回的集合进行判断
        if (CollectionUtils.isEmpty(tbClues)){
            log.info("线索暂时没有可以满足条件的人");
            return;
        }
//        具有满足条件的人
        for (TbClue tbClue : tbClues) {
            SysUser sysUser = sysUserMapper.selectUserById(tbClue.getUserId());
            //对于人进行查询是否已经发送过邮件
            Object o = redisTemplate.opsForValue().get(CLUE_NOTIFY_PREFIX + tbClue.getId());
            if (!ObjectUtils.isEmpty(o)){
                //已经发送过了
                log.info("已经发过了");
                continue;
            }
            log.info("开始发送邮件");
           //加载文件模板
            String file = this.getClass().getClassLoader().getResource("templates/ClueNotifyTemplate.html").getFile();
            String text = FileUtils.readFileToString(new File(file),"UTF-8");
            //填充数据
            String format = String.format(text, sysUser.getRealName(), tbClue.getId(), tbClue.getName(), tbClue.getPhone(), (sysUser.getSex().equals(0) ? "男" : "女"));
            //编辑邮件
             emailUtils.sendMailWithoutAttachment(mailFrom,sysUser.getEmail(),"测试邮件",format);
           //储存发送的标识
            log.info("发送成功");
            redisTemplate.opsForValue().set(CLUE_NOTIFY_PREFIX + tbClue.getId(), "1", 150, TimeUnit.MINUTES);
        }
    }
    //开始商机的提醒
    @Scheduled(cron = "0 * * * * ?")
    public void sendbusimail() throws Exception {
        //首先在商机的表内查询是否有符合的商机
      List<TbBusiness> tbBusinessList=  tbBusinessMapper.selectCondition(LocalDateTime.now(),LocalDateTime.now().plusHours(2));
       //判断是否为空
        if (CollectionUtils.isEmpty(tbBusinessList)){
            log.info("商机没有满足条件的");
            return;
        }
        //查看商机对应的人
        for (TbBusiness tbBusiness : tbBusinessList) {
            SysUser sysUser = sysUserMapper.selectUserById(tbBusiness.getUserId());
            Object o = redisTemplate.opsForValue().get(BUSINESS_NOTIFY_PREFIX + tbBusiness.getId());
//            if (!ObjectUtils.isEmpty(o)){
//                //已经发送过了
//                log.info("已经发过了");
//                continue;
//            }
            log.info("开始发送邮件");
            String file = this.getClass().getClassLoader().getResource("templates/BusinessNotifyTemplate.html").getFile();
            String text = FileUtils.readFileToString(new File(file), "UTF-8");
            String format = String.format(text, sysUser.getRealName(), tbBusiness.getId(),tbBusiness.getName() , tbBusiness.getPhone(), (tbBusiness.getSex().equals(0) ? "男" : "女"));
            emailUtils.sendMailWithoutAttachment(mailFrom,sysUser.getEmail(),"商机测试邮件",format);
            log.info("邮件发送成功");
            //开始rediskey
            redisTemplate.opsForValue().set(BUSINESS_NOTIFY_PREFIX+tbBusiness.getId(),"22",150,TimeUnit.MINUTES);
        }
    }
    //每月的最后一天晚上11点整 ，统计本月新增线索数、新增商机数、新增合同数、销售金额(合同金额)，并发送邮件到管理员(自己的)的邮箱，
    // 且以表格的形式展示出来 。 【必须】
    //注入管理员邮箱
    @Autowired
    private Configuration configuration;
    @Autowired
    private TbContractMapper tbContractMapper;
    private String adminEmail="934922791@qq.com";
   @Scheduled(cron = "0 0 11 L * ?")
   //@Scheduled(cron = "0/10 * * * * ?")
    public void  sendMailInMonthLast() throws Exception {
     //对于本月新增线索数、新增商机数、新增合同数、销售金额(合同金额)进行查询
        //传入本月时间到上月时间段
        LocalDateTime end = LocalDateTime.now();
        LocalDateTime begin = LocalDateTime.now().minusMonths(1);
        LocalDateTime beforeBegin = LocalDateTime.now().minusMonths(2);
        Long clueCount = tbClueMapper.selectCountWithNew(begin, end);
        Long businessCount = tbBusinessMapper.selectCountWithNew(begin, end);
        Long contractCount = tbContractMapper.selectCountWithNew(begin, end);
        double Money = tbContractMapper.selectMoneyByNew(begin, end);
        Long beforeClueCount = tbClueMapper.selectCountWithNew(beforeBegin, begin);
        Long beforeBusinessCount = tbBusinessMapper.selectCountWithNew(beforeBegin, begin);
        Long beforeContractCount = tbContractMapper.selectCountWithNew(beforeBegin, begin);
       double beforeMoney = tbContractMapper.selectMoneyByNew(beforeBegin, begin);
       //计算两个本月和上月的差值
       Long differenceClue = clueCount-beforeClueCount;
       Long differenceBusiness = businessCount-beforeBusinessCount;
       Long differenceContract = contractCount-beforeContractCount;
       double differenceMoney = Money-beforeMoney;
       //开始拼接
       String file = this.getClass().getClassLoader().getResource("templates/Table.html").getFile();
       String text = FileUtils.readFileToString(new File(file), "UTF-8");
       String format = String.format(text, clueCount, differenceClue.doubleValue()*100 / beforeClueCount,
               businessCount, differenceBusiness.doubleValue() *100/ beforeBusinessCount,
               contractCount, differenceContract.doubleValue()*100 / beforeContractCount,
               Money, differenceMoney*100.0 / beforeMoney);
       emailUtils.sendMailWithoutAttachment(mailFrom,"2798696233@qq.com","邮件",format);
   }
   //开发邮件统计每月每天的数目
    @Scheduled(cron = "0 0 0 L * ?")
   // @Scheduled(cron = "0/10 * * * * ?")
    public void sendEveryMothMail() throws Exception {
       //获取到每月每天的数目取得当前日期
        log.info("获取到每月每天的数目取得当前日期");
        LocalDate now = LocalDate.now();
        //取得当前月份第一天
        LocalDate beginLocalDate = now.withDayOfMonth(1);
        LocalDate endLocalDate = now.withDayOfMonth(now.lengthOfMonth());
        //拿到两个时间
        LocalDateTime begin = LocalDateTime.of(beginLocalDate, LocalTime.MIN);
        LocalDateTime end = LocalDateTime.of(endLocalDate, LocalTime.MAX);
        //拿到日期集合
        List<String> dateList = beginLocalDate.datesUntil(endLocalDate.plusDays(1)).map(date -> DateTimeFormatter.ofPattern("yyyy-MM-dd").format(date)).collect(Collectors.toList());
        //填入方法中得到值
   List<TbNumer>  tbClub =  tbClueMapper.selectClubNwe(begin,end);
        Map<String, Long> collect = tbClub.stream().collect(Collectors.toMap(TbNumer::getDate, TbNumer::getCount));
        List<TbNumer>  tbBusiness =  tbBusinessMapper.selectBusinessNwe(begin,end);
        Map<String, Long> collect1 = tbBusiness.stream().collect(Collectors.toMap(TbNumer::getDate, TbNumer::getCount));
        List<TbNumer>  tbContract =  tbContractMapper.selectContractNwe(begin,end);
        Map<String, Long> collect2 = tbContract.stream().collect(Collectors.toMap(TbNumer::getDate, TbNumer::getCount));
        List<ContarctMoney>  tbMoney =  tbContractMapper.selectMoneyNwe(begin,end);
        Map<String, String> collect3 = tbMoney.stream().collect(Collectors.toMap(ContarctMoney::getDate, ContarctMoney::getMoney));

         List<SendMailDTO> itemList = new ArrayList<>();
        //开始取得
        for (String s : dateList) {
            itemList.add(SendMailDTO.builder().date(s)
                    .newClueCount(collect.get(s)==null?0:collect.get(s))
                    .newBusinessCount(collect1.get(s)==null?0:collect1.get(s))
                    .newContractCount(collect2.get(s)==null?0:collect2.get(s))
                    .saleMoney(collect3.get(s)==null?0L:Long.parseLong(collect3.get(s))).build()) ;
        }
        Template template = configuration.getTemplate("Month.ftl");
        Map<String,Object> dataModel = new HashMap<>();
        dataModel.put("itemList",itemList);
        String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, dataModel);
        emailUtils.sendMailWithoutAttachment(mailFrom,"2798696233@qq.com", "no.create.permission", text);
    }
    //使用poi发送带有附件的邮件
    //@Scheduled(cron = "0 0 23 L * ?")
   @Scheduled(cron = "0/10 * * * * ?")
    public void sendMailWithAttach() throws Exception {
       //计算当前的日期
        LocalDate now = LocalDate.now();
        LocalDate beginDate = now.minusMonths(1).plusDays(1);
        //计算两个时间的差值
        LocalDateTime begin = LocalDateTime.of(beginDate, LocalTime.MIN);
        LocalDateTime end = LocalDateTime.of(now, LocalTime.MAX);
        //准备数据
        //日期
        List<String> dateList = beginDate.datesUntil(now.plusDays(1)).map(date -> DateTimeFormatter.ofPattern("yyyy-MM-dd").format(date)).collect(Collectors.toList());
        //新增线索
        List<TbNumer> tbNumerClue = tbClueMapper.selectClubNwe(begin, end);
        //总线索数
        List<Long> collect = tbNumerClue.stream().map(TbNumer::getCount).collect(Collectors.toList());
        Long allClub = collect.stream().reduce(Long::sum).get();
        //遍历每个线索
        Map<String, Long> collect4 = tbNumerClue.stream().collect(Collectors.toMap(TbNumer::getDate, TbNumer::getCount));
        List<Long> everClub = dateList.stream().map(date -> {
            return collect4.get(date) == null ? 0 : collect4.get(date);
        }).collect(Collectors.toList());
        //新增商机
        List<TbNumer> tbNumersBusiness = tbBusinessMapper.selectBusinessNwe(begin, end);
        //总商机
        List<Long> collect1 = tbNumersBusiness.stream().map(TbNumer::getCount).collect(Collectors.toList());
        Long allBusiness = collect1.stream().reduce(Long::sum).get();
        //遍历每个商机
        Map<String, Long> collect6 = tbNumersBusiness.stream().collect(Collectors.toMap(TbNumer::getDate, TbNumer::getCount));
        List<Long> everBusiness = dateList.stream().map(date -> {
            return collect6.get(date) == null ? 0 : collect6.get(date);
        }).collect(Collectors.toList());
        //新增合同
        List<TbNumer> contractNwe = tbContractMapper.selectContractNwe(begin, end);
        //总新增合同
        List<Long> collect2 = contractNwe.stream().map(TbNumer::getCount).collect(Collectors.toList());
        Long allContract = collect2.stream().reduce(Long::sum).get();
        //遍历每个合同
        Map<String, Long> collect7 = contractNwe.stream().collect(Collectors.toMap(TbNumer::getDate, TbNumer::getCount));
        List<Long> everContract = dateList.stream().map(date -> {
            return collect7.get(date) == null ? 0 : collect7.get(date);
        }).collect(Collectors.toList());
        //销售额
        List<ContarctMoney> contarctMonies = tbContractMapper.selectMoneyNwe(begin, end);
        //总销售额
        List<Double> collect3 = contarctMonies.stream().map(mailFrom->Double.valueOf(mailFrom.getMoney())).collect(Collectors.toList());
        double allMoney = collect3.stream().reduce(Double::sum).get();
        //遍历每日销售额
        Map<String, String> collect8 = contarctMonies.stream().collect(Collectors.toMap(ContarctMoney::getDate, ContarctMoney::getMoney));
        List<String> everMoney= dateList.stream().map(date -> {
            return collect8.get(date) == null ? "0" : collect8.get(date);
        }).collect(Collectors.toList());
        String head =beginDate+"到"+now+"之间的运营数据统计";
        //开始对上面数据进行拼接
        InputStream input = this.getClass().getClassLoader().getResourceAsStream("templates/客达天下-月度运营统计.xlsx");
        Workbook workbook = new XSSFWorkbook(input);
        Sheet sheet = workbook.getSheetAt(0);
        //设置时间
        sheet.getRow(1).getCell(1).setCellValue(head);
        //设置各种总数
        Row row3 = sheet.getRow(3);
        row3.getCell(1).setCellValue(allClub);
        row3.getCell(2).setCellValue(allBusiness);
        row3.getCell(3).setCellValue(allContract);
        row3.getCell(4).setCellValue(allMoney);
        //开始设置循环列表
        for (int i = 5; i < dateList.size()+5; i++) {
            Integer index = i-5;
            Row row = sheet.getRow(i);
            row.getCell(0).setCellValue(index+1);
            row.getCell(1).setCellValue(dateList.get(index));
            row.getCell(2).setCellValue(everClub.get(index));
            row.getCell(3).setCellValue(everBusiness.get(index));
            row.getCell(4).setCellValue(everContract.get(index));
            row.getCell(5).setCellValue(everMoney.get(index));
        }
        //保存本地
        FileOutputStream out =new FileOutputStream(new File("F://测试.xlsx"));
        workbook.write(out);
        //开始发送邮件
        emailUtils.sendMailWithAttachment(mailFrom,"2798696233@qq.com", "no.create.permission","这是一封邮件.xlsx","F://测试.xlsx","这是一封邮件" );
        //释放资源
        input.close();
        out.close();
        workbook.close();

    }
}
